package INF102.lab5.graph;

import java.util.*;

public class AdjacencySet<V> implements IGraph<V> {
    private HashSet<V> vertices;
    private HashMap<V, HashSet<V>> adjacencyList;

    /**
     * Construct an empty graph
     */
    public AdjacencySet() {
        this.vertices = new HashSet<V>();
        this.adjacencyList = new HashMap<V, HashSet<V>>();
    }

    @Override
    public int size() {
        return vertices.size();
    }

    @Override
    public void addNode(V node) {
        if(vertices.contains(node)){
            throw new IllegalArgumentException();
        }
        vertices.add(node);
    }

    @Override
    public void removeNode(V node) {
        if (!vertices.contains(node)) {
            throw new IllegalArgumentException();
        }
        vertices.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        if(!hasEdge(u,v)){
            adjacencyList.put(u, getNeighbourhood(u));
            adjacencyList.put(v, getNeighbourhood(v));
        }
    }

    @Override
    public void removeEdge(V u, V v) {
        if(hasEdge(u,v)) {
            adjacencyList.get(v).remove(u);
            adjacencyList.get(u).remove(v);
        }
    }

    @Override
    public boolean hasNode(V node) {

        return vertices.contains(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return (getNeighbourhood(u).contains(v) && getNeighbourhood(v).contains(u));
    }

    @Override
    public HashSet<V> getNeighbourhood(V node) {
        if(!adjacencyList.containsKey(node)){
            return new HashSet<>();
        }
        return adjacencyList.get(node);

    }

    @Override
    public boolean connected(V u, V v) {
        LinkedList<V> queue = new LinkedList<>();
        HashSet<V> visited = new HashSet<>();
        queue.add(v);
        while(!queue.isEmpty()){
            V n = queue.removeFirst();
            HashSet<V> n_neigb = getNeighbourhood(n);
            visited.add(n);
            for (V node:n_neigb){
                if(node == u){
                    return true;
                }
                if(!visited.contains(node)){
                    if(!queue.contains(node)){
                        queue.add(node);
                    }
                }
            }

        }
        return false;

    }
}
